import React, { useState } from "react";

import List from "./components/List";

import "./App.css";

function App() {
  const [personalComment, setPersonalComment] = useState("");
  const [comments, setComments] = useState([
    {
      id: 1,
      user: "realuser1",
      comment:
        "The only interesting video would be of her identifying a defect.",
      parent_id: null,
    },
    {
      id: 2,
      user: "xc123",
      comment:
        "She does a front flip and some power ranger moves and punches the bottle",
      parent_id: 1,
    },
    {
      id: 3,
      user: "tgi52",
      comment: "But she did, watch her eyes at 0:02 seconds",
      parent_id: 1,
    },
    {
      id: 4,
      user: "potus",
      comment: "Love power rangers!",
      parent_id: 2,
    },
    {
      id: 5,
      user: "spartacus",
      comment: "I don't.",
      parent_id: 4,
    },
  ]);

  const addComment = (comment) => {
    let newList = [...comments, comment];

    setComments(newList);
  };

  return (
    <div className="App">
      <p>
        Commenting as <strong>gerdedja</strong>
      </p>
      <textarea
        style={{ width: "500px", height: "250px", display: "block" }}
        placeholder="What are your thoughts?"
        value={personalComment}
        onChange={(e) => setPersonalComment(e.target.value)}
      />
      <button
        onClick={() => {
          if (personalComment) {
            addComment({
              id: comments.length + 1,
              user: "gerdedja",
              comment: personalComment,
              parent_id: null,
            });
            setPersonalComment("");
          }
        }}
      >
        Comment
      </button>
      <h1>Replies</h1>
      <List comments={comments} addComment={addComment} />
    </div>
  );
}

export default App;
