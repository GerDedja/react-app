import React from "react";
import Comment from "./Comment";
import { nest } from "../utils";

const List = ({ comments, addComment }) => {
  const nestedComments = nest(comments);

  return (
    <ul>
      {nestedComments.map((comment) => (
        <Comment
          comments={comments}
          addComment={addComment}
          parentId={comment.id}
          {...comment}
        />
      ))}
    </ul>
  );
};

export default List;
