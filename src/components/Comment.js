import React, { useState } from "react";

const Comment = (props) => {
  const [comment, setComment] = useState("");

  return (
    <li>
      <p>
        <span style={{ fontSize: "13px" }}>{props.user}</span>: {props.comment}
      </p>
      <input
        value={comment}
        onChange={(e) => setComment(e.target.value)}
        placeholder={`Reply to ${props.user}`}
      />
      <button
        onClick={() => {
          props.addComment({
            id: props.comments.length + 1,
            user: "gerdedja",
            comment,
            parent_id: props.id,
          });
          setComment("");
        }}
      >
        Reply
      </button>
      <ul>
        {props.children.map((child) => (
          <Comment
            comments={props.comments}
            addComment={props.addComment}
            parentId={props.id}
            {...child}
          />
        ))}
      </ul>
    </li>
  );
};

export default Comment;
