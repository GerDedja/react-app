const nest = (items, id = null) => {
  return items
    .filter((item) => item.parent_id === id)
    .map((item) => ({
      ...item,
      children: nest(items, item.id),
    }));
};

export { nest };
